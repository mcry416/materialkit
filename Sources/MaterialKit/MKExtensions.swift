//
//  File.swift
//  
//
//  Created by Eldest's MacBook on 2023/12/21.
//

import UIKit

@available(iOSApplicationExtension, unavailable)
@available(watchOSApplicationExtension, unavailable)
@available(tvOSApplicationExtension, unavailable)
@available(macCatalystApplicationExtension, unavailable)
@available(OSXApplicationExtension, unavailable)
extension UIApplication {
    class var this: UIApplication? {
        return UIApplication.shared
    }
}
